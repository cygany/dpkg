```
upstream git@salsa.debian.org:dpkg-team/dpkg.git
origin   git@gitlab.com:cygany/dpkg.git
```

Compile with:

(Needs to be clean, use make distclean or git clean -d -f -x)

```
./autogen
./configure
make -j8
```

Compile with itself:

(After dpkg is in PATH, and debhelper debs are installed with it):

Autoconf config.sub (build-aux/config.sub) needs a patch.
Modified file copied to /usr/share/autoconf2.7/build-aux/config.sub

```
PERL5LIB=/usr/share/perl5/vendor_perl/5.32:/usr/share/perl5 debian/rules build
```

Then create the deb by

```
PERL5LIB=/usr/share/perl5/vendor_perl/5.32:/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules binary
```
